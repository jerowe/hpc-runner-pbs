#!/bin/bash
#
#PBS -N job_1
#PBS -q interactive
#PBS -l nodes=2:ppn=4
#PBS -o localhost:/home/jillian/projects/perl/HPC-Runner-PBS/script/logs/pbs_logs_2015_10_19T08_52_12/job_1.log
#PBS -e localhost:/home/jillian/projects/perl/HPC-Runner-PBS/script/logs/pbs_logs_2015_10_19T08_52_12/job_1.log


cd /home/jillian/projects/perl/HPC-Runner-PBS/script
mcerunner.pl --procs 4 --infile /home/jillian/projects/perl/HPC-Runner-PBS/script/logs/job_1.in --outdir /home/jillian/projects/perl/HPC-Runner-PBS/script/logs --logname job_1
